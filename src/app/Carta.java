package app;

import java.util.StringTokenizer;

/**
 * Clase utilizada para la definici�n de un carta, en este caso, de la baraja espa�ola
 */
public class Carta implements Comparable<Carta>{

	protected int numero;
	protected Palo palo;
	protected int valoracion = 0;
/**
 * Este constructor crea una carta de la baraja espa�ola
 * @param num Numero de la carta
 * @param p Palo de la carta
 */public Carta(){
	 
 	}
	public Carta(int num,Palo p){
		switch(num){
		case 8:
			numero = 10;
		break;
		case 9:
			numero = 11;
		break;
		case 10:
			numero = 12;
		break;
		default:
			numero = num;
		break;
		}
		palo = p;
	}
/**
 * 
 * @return Devuelve el numero de la carta
 */
	public int getNumero(){
		return numero;
	}
/**
 * 
 * @return Devuelve el palo de la carta
 */
	public Palo getPalo(){
		return palo;
	}
/**
 * 
 * @return Valoracion de una carta
 */
	public int getValoracion(){
		return valoracion;
	}
/**
 * Metodo para pasar de un String a una Carta
 * @param String carta
 * @return Carta
 */
	public static Carta[] str2Carta(String cartas) {
		StringTokenizer s = new StringTokenizer(cartas," \ny,");
		Carta[] aux = new Carta[4];
		int i = 0;
		String token = "";
		while(s.hasMoreTokens()){
			int  numero = 0;
			Palo palo;
			token = s.nextToken();
			numero = Integer.parseInt(token);
			s.nextToken();
			token = s.nextToken();
			palo = new Palo(token);
			if(numero == 10)
				aux[i] = new Carta(8,palo);
			else
				aux[i] = new Carta(numero,palo);
			i++;
		}
		return aux;
	}
/**
 * Metodo para saber si un string es una Carta
 * @param carta string de la posible carta
 * @return true si es una carta
 */
	public static boolean esCarta(String carta) {
		boolean esCarta = true;
		StringTokenizer s = new StringTokenizer(carta," \ny,");
		String token ="";
		if(!carta.equals("") && s.countTokens() % 3 != 0){
			esCarta = false;
		}
		while(s.hasMoreTokens() && esCarta){
			token = s.nextToken();
			try{
				Integer.parseInt(token);
			}catch(NumberFormatException e){
				esCarta = false;
			}
			s.nextToken();
			token = s.nextToken();
			if(!token.equals("bastos") && !token.equals("oros") && !token.equals("espadas") && !token.equals("copas")){
				esCarta = false;
			}
		}
		return esCarta;
	}
/**
 * Igualdad de cartas
 * @param c Carta para comparar
 * @return true si son iguales
 */
	
	public boolean equals(Object c){
		return c instanceof Carta && ((Carta)c).numero == this.numero && this.palo.equals(((Carta)c).palo);
		
	}
	public int hashCode(){
		return numero + palo.hashCode();
	}
/**
 * 
 * @return Devuelve una representacion de una carta ej. 1 de oros
 */
	public String toString(){
		StringBuilder s =new StringBuilder("");
		s = s.append(numero).append(" de ").append(palo.toString());
		return s.toString();
	}
	public int compareTo(Carta c) {
		int res = 0;
		if(valoracion == c.valoracion){
			if(numero < c.numero){
				res = -1;
			}else if(numero > c.numero){
				res = 1;
			}else{
				res = palo.compareTo(c.getPalo());
			}
		}else{
			if(valoracion < c.valoracion){
				res = -1;
			}else if(valoracion > c.valoracion){
				res = 1;
			}else{
				res = palo.compareTo(c.getPalo());
			}
		}
		 return res;
	}
}
