package app;

import java.util.Arrays;
import java.util.Random;
/**
 * Un mazo es el conjunto de todas las cartas que existen
 * @author Fran
 *
 */
public class Mazo {
	private final int 	NUM_CARTAS = 40;
	private  int   ULTIMA = 39;
	private Carta[] mazo = new Carta[NUM_CARTAS];
	private Random r = new Random();
/**
 * Constructor de un mazo a traves de los descartes
 * @param m mazo de descartes
 */
	public Mazo(Carta[] m){
		mazo = m;
	}
	/**
	 * Constructor de un mazo, en este caso es una baraja espa�ola del 1 al 7 con 4 palos
	 */
	public Mazo(){
		for(int i = 0;i<10;i++){
			mazo[i] = new Carta(i+1,new Palo("oros"));
		}
		for(int i = 0;i<10;i++){
			mazo[i+10] = new Carta(i+1,new Palo("copas"));
		}
		for(int i = 0;i<10;i++){
			mazo[i+20] = new Carta(i+1,new Palo("espadas"));
		}
		for(int i = 0;i<10;i++){
			mazo[i+30] = new Carta(i+1,new Palo("bastos"));
		}
	}
/**
 * Barajar el mazo
 */
	public void barajar(){
		Carta aux;
		for(int i = 0;i<mazo.length;i++){
			int n = r.nextInt(NUM_CARTAS);
			aux = mazo[i];
			mazo[i] = mazo[n];
			mazo[n] = aux;
		}
	}
/**
 * Metodo para repartir
 * @return Devuelve la ultima carta del mazo
 */
	public Carta darCarta(){
		if(ULTIMA < 0){
			throw new RuntimeException("No hay cartas en el mazo");
		}else{
			Carta take;
			take = mazo[ULTIMA];
			ULTIMA--;
			return take;
		}
	}
/**
 * Metodo para ver la ultima carta
 * @return Devuelve la ultima carta del mazo sin eliminarla del mazo
 */
	public Carta verCarta(){
		return mazo[ULTIMA];
	}
/**
 * Un mazo esta vacio si no tiene cartas
 * @return
 */
	public boolean isEmpty(){
		return ULTIMA < 0;
	}
/**
 * Un Mazo es el conjunto de cartas que tiene sin coger
 */
	public String toString(){
		StringBuilder s = new StringBuilder("");
		for(int i = 0;i<ULTIMA+1;i++){
			s=s.append(mazo[i].toString()).append(",");
		}
		return s.toString();
	}
}
