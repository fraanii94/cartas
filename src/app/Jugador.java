package app;

import culo.CartaCulo;

public interface Jugador {

/**
 * Comprueba si la mano de un jugador esta vac�a
 * @return true si esta vacia
 */
	public boolean manoVacia();
/**
 * Devuelve la mano del jugador
 * @return mano del jugador
 */
	public Carta[] getMano();
/**
 * Asignamos al jugador una mano de cartas
 * @param m mano para asignar
 */
	public void setMano(Carta[] m);
	
}
