package app;

public class Palo implements Comparable<Palo>{

	private String palo;
	
	public Palo(String p){
		palo = p.toLowerCase();
	}
	public boolean equals(Object p){
		return p instanceof Palo && this.palo.equals(((Palo)p).palo);
	}
	public int hashCode(){
		return palo.hashCode();
	}
	public String toString(){
		return palo;
	}
	public int compareTo(Palo p){
		return palo.compareTo(p.palo);
	}
}
