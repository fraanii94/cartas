package culo;

import app.Carta;
import app.Palo;

public class CartaCulo extends Carta{
/**
 * Constructor de una CartaCulo a partir de una Carta
 * @param c Carta
 */
	public CartaCulo(Carta c){
		numero = c.getNumero();
		palo = c.getPalo();
		darValoracion(numero);
	}
/**
 * Constructor de una CartaCulo
 * @param num numero
 * @param p palo
 */
	public CartaCulo(int num, Palo p) {
		super(num, p);
		darValoracion(num);
	}
/**
 * Metodo para asignar la valoracion de las CartaCulo
 * @param num valoracion
 */
	private void darValoracion(int num) {
		switch(num){
		case 1:
			valoracion = 9;
		break;
		case 2:
			if(palo.equals("copas"))
				valoracion = 11;
			else
				valoracion = 10;
		break;
		case 3:
			valoracion = 1;
		break;
		case 4:
			valoracion = 2;
		break;
		case 5:
			valoracion = 3;
		break;
		case 6:
			valoracion = 4;
		break;
		case 7:
			valoracion = 5;
		break;
		case 10:
			valoracion = 6;
		break;
		case 11:
			valoracion = 7;
		break;
		case 12:
			valoracion = 8;
		break;
		}	
	}
/**
 * Metodo para pasar de String a Carta llama al metodo de la clase Carta
 * @param c String
 * @return una Carta
 */
	public static CartaCulo[] str2Carta(String c){
		CartaCulo[] aux =toArrayCartaCulo(Carta.str2Carta(c));
		return aux;
	}
	private static CartaCulo[] toArrayCartaCulo(Carta[] str2Carta) {
		CartaCulo[] aux = new CartaCulo[4];
		int i = 0;
		while(str2Carta[i] != null && i< str2Carta.length){
			aux[i] = new CartaCulo(str2Carta[i]);
			i++;
		}
		return aux;
	}
/**
 * Metodo equals de la clase CartaCulo
 * @param c Cartaculo a comparar
 * @return booleano
 */
	public boolean equals(Object c){
		return c instanceof CartaCulo && super.equals(c) && valoracion == ((CartaCulo)c).valoracion;
	}
	public int hashCode(){
		return super.hashCode() + valoracion;
	}

}
