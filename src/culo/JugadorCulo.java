package culo;

import java.util.Arrays;

import app.*;

/**
 * Designa el jugador para el Culo
 * @author Fran
 *
 */
public class JugadorCulo implements Jugador{
	private String funcion = null;
	private CartaCulo[] mano;
	
	public JugadorCulo(){
		
	}
/**
 *  Constructor para crear el jugador culo, lo crea el padre
 * @param func para asignar funcion desde el principio
 */
	public JugadorCulo(String func) {
		funcion = func;
	}
/**
 * 
 * @return devuelve el resultado de la anterior partida del jugador
 */
	public String getFuncion(){
		return funcion;
	}
/**
 * Asigna el resultado de la partida al jugador
 * @param func resultado de la partida
 */
	public void setFuncion(String func){
		funcion = func;
	}
	@Override
	public boolean manoVacia() {
		
		return mano.length == 0;
	}
	@Override
	public CartaCulo[] getMano() {
		
		return mano;
	}
	@Override
	public void setMano(Carta[] m) {
		mano = (CartaCulo[]) m;
		
	}
	public String toString(){
		return Arrays.toString(mano);
	}
}
