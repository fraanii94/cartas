package culo;

import java.util.Arrays;

import app.Carta;

public  class EchaCartas {

/**
 * Metodo para echar 4 cartas
 * @param carta
 * @param carta2
 * @param carta3
 * @param carta4
 * @param j
 */
	public static boolean echarCuatroCartas(CartaCulo carta, CartaCulo carta2, CartaCulo carta3,CartaCulo carta4,JugadorCulo j) {
		CartaCulo aux[] ={carta,carta2,carta3,carta4};
		return echarCartas(aux,j);
	}
/**
 * metodo para echar 3 cartas
 * @param carta
 * @param carta2
 * @param carta3
 * @param j
 */
	public static boolean echarTresCartas(CartaCulo carta, CartaCulo carta2, CartaCulo carta3,JugadorCulo j) {
		CartaCulo aux[] ={carta,carta2,carta3};
		return echarCartas(aux,j);
	}
/**
 * metodo para echar 2 cartas
 * @param carta
 * @param carta2
 * @param j
 */
	public static boolean echarDosCartas(CartaCulo carta, CartaCulo carta2,JugadorCulo j) {
		CartaCulo aux[] ={carta,carta2};
		return echarCartas(aux,j);
	}
/**
 * Metodo para echar 1 carta
 * @param carta a echar
 * @param j jugador que echa la carta
 */
	public static boolean echarUnaCarta(CartaCulo carta,JugadorCulo j) {
		CartaCulo aux[] ={carta};
		return echarCartas(aux,j);
	}
/**
 * Metodo auxiliar para echar cartas
 * @param cartas
 * @param j
 */
	private static boolean echarCartas(CartaCulo[] cartas,JugadorCulo j){
		boolean ok = true;
		int i = 0;
		
		while(ok && i < cartas.length){
			int idx = Arrays.binarySearch(j.getMano(), cartas[i]);
			if(!j.getMano()[idx].equals(cartas[i])){
				System.err.println("No tienes esta carta en la mano");
				System.err.flush();
				System.out.println(Arrays.toString(j.getMano())+cartas[i].toString()+idx);
				ok = false;
			}else{
				CartaCulo[] aux = new CartaCulo[j.getMano().length -1];
				j.getMano()[idx] = null;	
				for(int k = idx;k<j.getMano().length-1;k++){
					j.getMano()[k] = j.getMano()[k+1];
				}
				System.arraycopy(j.getMano(), 0, aux, 0, aux.length);
				j.setMano(aux);
			}
			i++;
		}
		return ok;
	}
}
