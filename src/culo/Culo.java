package culo;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

import app.*;
/**
 * Clase para el juego Culo
 * @author Fran
 *
 */
public class Culo {

	private Mazo mazo;
	private JugadorCulo[] jugadores;
	private List<CartaCulo> descartes = new LinkedList<CartaCulo>();
	private CartaCulo mayor = new CartaCulo(0,new Palo("oros"));
	private boolean[] jugada = new boolean[4];
	private String[] funciones = new String[4];
	private int funcs = 1;
	public final CartaCulo DOSDECOPAS= new CartaCulo(2,new Palo("copas"));
/**
 * Constructor, lo que hace es repartir todas las cartas entre todos los jugadores
 * @param jug numero de jugadores para jugar al culo
 */
	public Culo(int jug){
		jugadores = new JugadorCulo[jug];
		mazo = new Mazo();
	}
/**
 * Inicializacion del juego
 */
	public void start(){
		repartir();
	}
/**
 * 
 * @return jugadores del culo
 */
	public JugadorCulo[] getJugadores(){
		return jugadores;
	}
/**
 * Crea las manos de cada jugador, en el constructor de Jugador se reparten las cartas
 */
	public void repartir() {
		mazo.barajar();
		if(40 % jugadores.length == 0){
			for(int i = 0;i<jugadores.length;i++){
				jugadores[i] = new JugadorCulo();
				repartirAJugador(40/jugadores.length, jugadores[i],mazo);
				Arrays.sort(jugadores[i].getMano());
			}
		}else{
			for(int i = 0;i<(40%jugadores.length);i++){
				jugadores[i] = new JugadorCulo();
				repartirAJugador((40/jugadores.length)+1, jugadores[i],mazo);
				Arrays.sort(jugadores[i].getMano());
			}
			for(int i = (40%jugadores.length);i<jugadores.length;i++){
				jugadores[i] = new JugadorCulo();
				repartirAJugador(40/jugadores.length, jugadores[i],mazo);
				Arrays.sort(jugadores[i].getMano());
			}
		}	
	}
/**
 * Metodo para repartir cartas a un jugador
 * @param num numero de cartas a repartir
 * @return Devuelve un numero de cartas
 */
		private void repartirAJugador(int num,JugadorCulo j,Mazo m){
			CartaCulo[] mano = new CartaCulo[num];
			for(int i = 0;i<num;i++){
				mano[i] = new CartaCulo(m.darCarta());
			}
			j.setMano(mano);
			
		}
/**
 * @param jug indice de la jugada a echar 0 = 1 carta, 1 = 2 cartas, 2 = 3 cartas, 3 = 4 cartas
 */
	public void setJugada(int jug){
		for(int i = 0;i < jugada.length;i++){
			if(i == (jug-1)){
				jugada[i] = true;
			}else{
				jugada[i] = false;
			}
		}
	}
/**
 * @return jugada a echar
 */
	public int getJugada(){
		int jug = 0;
		while( jug < jugada.length && !jugada[jug] ){
			jug++;
		}
		if(jug >= jugada.length){
			jug = 0;
		}else{
			jug++;
		}
		return jug;
	}
/**
 * @return devuelve el mazo de descartes
 */
	public List<CartaCulo> getDescartes(){
		return descartes;
	}
/**
 * @return mayor carta echada
 */
	public CartaCulo getMayor(){
		return mayor;
	}

	public boolean esCarta(String carta){
		return CartaCulo.esCarta(carta);
	}
/**
 * Metodo para poner ultima carta echada por un jugador
 * @param c Carta mayor echada
 * @param jug numero de cartas echadas
 */
	public void setMayor(CartaCulo c,int jug){
		if(!jugada[jug-1]){
			throw new RuntimeException("Esta no es la jugada actual");
		}	
		if(mayor.compareTo(c) <= 0)
			mayor = c;
		else if(mayor.compareTo(c) > 0)
			throw new RuntimeException("Esta carta no puede ser echada");
		else{
			if(mayor.getValoracion() <= c.getValoracion())
				mayor = c;
			else
				throw new RuntimeException("Esta carta no puede ser echada");
		}
	}
/**
 * 
 * @return Devuelve true si ha terminado la partida
 */
	public boolean finDePartida() {
		boolean fin = true;
		int i = 0;
		while(fin && i<jugadores.length){
			if(!jugadores[i].manoVacia()){
				fin = false;
			}
			i++;
		}
		return fin;
	}
/**
 * Coge el mazo de descartes de la jugada y lo a�ade a descartes
 * @param c descartes
 */
	public void cerrarJugada(LinkedList<CartaCulo> c){
		for(int i = 0;i < jugada.length;i++){
			if(jugada[i])
				jugada[i] = false;
		}
		mayor = new CartaCulo(3,new Palo("oros"));
		descartes.addAll(c);
	}
/**
 * Funcion para asignar funcion al jugador que termino
 * @param jugadorCulo al que se le asigna el resultado
 */
	public void asignarFuncion(JugadorCulo jugadorCulo) {
		if(funcs == 1){
			jugadorCulo.setFuncion("presidente");
			System.out.println("Enhorabuena, eres el presidente!!!");
		}else if(funcs == 2){
			jugadorCulo.setFuncion("vicepresidente");
			System.out.println("Felicidades, eres el vicepresidente!!");
		}
		else if(funcs == jugadores.length-1){
			jugadorCulo.setFuncion("viceculo");
			System.out.println("Uff, te has salvado por poquito viceculo");
		}else if(funcs == jugadores.length){
			jugadorCulo.setFuncion("culo");
			System.out.println("Ooh, eres el culo de la partida");
		}else{
			jugadorCulo.setFuncion("ciudadano");
			System.out.println("Esta vez te salvaste, eres ciudadano!");
		}
		funcs++;
	}
/**
 * Devuelve la mano de todos los jugadores, metodo para testeo
 */
	public String toString(){
		return Arrays.toString(jugadores);
	}	
}